library IEEE;
	
use ieee.std_logic_1164.all;

use ieee.numeric_std.all;

entity ALU is

	port
	(
		-- Input ports
		opeA	: in  std_logic_vector(6 downto 0); -- depende del tamaño de los datos que vaya a usar
		opeB	: in  std_logic_vector(6 downto 0); -- depende del tamaño de los datos que vaya a usar
		operacion	: in  std_logic_vector(3  downto 0);

		-- Output ports
		respuesta	: out std_logic_vector(6 downto 0) -- depende del tamaño de los datos que vaya a usar
	);
end ALU;



architecture Behavioral of ALU is

signal myOperation : integer := 0;

begin
	
	myOperation <= to_integer(unsigned(operacion));

	with myOperation select
		respuesta <= std_logic_vector(signed(opeA) + signed(opeB)) when 0,  -- Aqui se declaran las operaciones que se deben hacer
						 std_logic_vector(signed(opeA)- signed(opeB)) when others;	

end Behavioral;

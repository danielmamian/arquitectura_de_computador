library IEEE;
	
use ieee.std_logic_1164.all;

use ieee.numeric_std.all;

entity Integracion is
    port(
		-- Input ports
		clk_in : in std_logic;
		weRF : in std_logic;
		opALU : in std_logic_vector(3 downto 0);
		muxALUSelector : in std_logic;
		muxIorDSelector : in std_logic;
		entradaPC : in natural range 0 to 63;
		weIR : in std_logic;
		weRAM : in std_logic;
		weRA : in std_logic;
		weRB : in std_logic;
		weALUOut : in std_logic;
		
		salidaResultado : out std_logic_vector(6 downto 0); -- Depende del resultado de la ALU
		queesrs : out std_logic_vector(3 downto 0);
		queesrt : out std_logic_vector(3 downto 0);
		queesrd : out std_logic_vector(3 downto 0);
		queesA : out std_logic_vector(6 downto 0);
		queesB : out std_logic_vector(6 downto 0);
		inst : out std_logic_vector(20 downto 0)
	 );
end Integracion;

architecture Behavioral of Integracion is


signal instruccionDeMemoria : std_logic_vector(20 downto 0);-- Depende del tamaño de los instrucciones que vaya a usar
signal resultadoALU : std_logic_vector(6 downto 0);-- Depende del tamaño de los datos que vaya a usar
signal datoA : std_logic_vector(6 downto 0); -- Depende del tamaño de los datos que vaya a usar
signal datoB : std_logic_vector(6 downto 0);-- Depende del tamaño de los datos que vaya a usar
signal resultadoMuxALU : std_logic_vector(6 downto 0);-- Depende del tamaño de los datos que vaya a usar
signal opSignal : std_logic_vector(3 downto 0); -- Depende del tamaño de los registros
signal rtSignal : std_logic_vector(3 downto 0); -- Depende del tamaño de los registros
signal rsSignal : std_logic_vector(3 downto 0); -- Depende del tamaño de los registros
signal rdSignal : std_logic_vector(3 downto 0); -- Depende del tamaño de los registros
signal resultadoMuxIorD : natural range 0 to 63; -- Depende de lo largo de la memoria
signal registroAOut : std_logic_vector(6 downto 0);
signal registroBOut : std_logic_vector(6 downto 0);
signal registroALUOut : std_logic_vector(6 downto 0);


	begin
		salidaResultado <= registroALUOut;
		queesrd <= rdSignal;
		queesrs <= rsSignal;
		queesrt <= rtSignal;
		inst <= instruccionDeMemoria;
		queesA <= registroAOut;
		queesB <= registroBOut;
		
		RAM : work.RAMemory
			port map(
				clk => clk_in,
				addr => resultadoMuxIorD,
				q => instruccionDeMemoria,
				we => weRAM, -- FALTA UNIDAD DE CONTROL
				data => "000000000000000000000" -- Depende del tamaño de la instruccion que vaya a usar
			);
			
		RegisterFile : work.registerFile
			port map(
				clk => clk_in,
				rd => rdSignal, 
				rs => rsSignal, 
				rt => rtSignal, 
				dataIn => resultadoALU,
				writeEneable => weRF,
				rOutA => datoA,
				rOutB => datoB
			);
		
		ALU : work.ALU
			port map(
				opeA => registroAOut,	
				opeB => resultadoMuxALU,
				operacion => opALU, -- FALTA UNIDAD DE CONTROL
				respuesta => resultadoALU
			);
			
		muxALU : work.muxALU
			port map(
				selector => muxALUSelector, -- FALTA UNIDAD DE CONTROL
				constanteInstruccion => "00" & instruccionDeMemoria(4 downto 0), -- Depende de como voy a decodificar la instruccion que viene de la memoria
				datoRegistro => registroBOut,
				datoElegido => resultadoMuxALU
			);
			
		IR: work.IRegister
			port map(
				-- Input ports
				memoryInstruction => instruccionDeMemoria,
				clk => clk_in,
				writeEneable => weIR, -- FALTA UNIDAD DE CONTROL
				opcode => opSignal,
				rdIR => rdSignal,
				rtIR => rtSignal,
				rsIR => rsSignal
			);
		
		muxIorD : work.muxMemoryIorD
			port map(
				selector => muxIorDSelector,
				instruccion => entradaPC,
				dato => registroALUOut,
				datoElegido => resultadoMuxIorD
			);
	
		RA : work.registro
			port map(
				entradaDato => datoA,
				clk => clk_in,
				writeEneable => weRA,
				salidaDato => registroAOut
			);
		RB : work.registro
			port map(
				entradaDato => datoB,
				clk => clk_in,
				writeEneable => weRB,
				salidaDato => registroBOut
			);
		ALUOut : work.registro
			port map(
				entradaDato => resultadoALU,
				clk => clk_in,
				writeEneable => weALUOut,
				salidaDato => registroALUOut
			);
end Behavioral;
library IEEE;
	
use ieee.std_logic_1164.all;

use ieee.numeric_std.all;

entity IRegister is
    port(
		-- Input ports
		memoryInstruction : in std_logic_vector(20 downto 0); -- Depende del tamaño de los instrucciones que vaya a usar
		clk : in std_logic;
		writeEneable : in std_logic;
		
		-- Output ports
		Opcode : out std_logic_vector(3 downto 0); -- Depende de como voy a decodificar la instruccion que viene de la memoria
		rdIR : out std_logic_vector(3 downto 0); -- Depende de como voy a decodificar la instruccion que viene de la memoria 
		rtIR : out std_logic_vector(3 downto 0); -- Depende de como voy a decodificar la instruccion que viene de la memoria
		rsIR : out std_logic_vector(3 downto 0) -- Depende de como voy a decodificar la instruccion que viene de la memoria
	 );
end IRegister;

architecture Behavioral of IRegister is
		signal instruccion : std_logic_vector(20 downto 0);
begin
	process(clk)
		begin
			if(falling_edge(clk)) then
				if(writeEneable = '1') then
					instruccion <= memoryInstruction;
				end if;
			end if;
			
			opcode <= instruccion(20 downto 17);-- Depende de como voy a decodificar la instruccion que viene de la memoria
			rdIR <= instruccion(8 downto 5);-- Depende de como voy a decodificar la instruccion que viene de la memoria
			rsIR <= instruccion(12 downto 9);-- Depende de como voy a decodificar la instruccion que viene de la memoria
			rtIR <= instruccion(16 downto 13);-- Depende de como voy a decodificar la instruccion que viene de la memoria 
	end process;
end Behavioral;
library IEEE;
	
use ieee.std_logic_1164.all;

use ieee.numeric_std.all;


entity controlUnit is

	port
	(
		-- Input ports
		op	: in  std_logic_vector(1 downto 0); -- Depende del numero de instrucciones que vaya a utilizar
		-- Output ports
		MemWrite	: out std_logic;
		RegWrite	: out std_logic;
		RegDat : out std_logic;
		ALUOp : out std_logic_vector(1 downto 0) -- Depende del numero de instrucciones que vaya a utilizar
	);
end controlUnit;


architecture Behavioral of controlUnit is

signal operacion : std_logic_vector(1 downto 0);

begin

operacion <= op;
	process(op)
			begin
				case op is 
					when "00" =>
						MemWrite <= '0';
						RegWrite <= '0';
						RegDat <= '0';
						ALUOp <= operacion;
					when "01" =>
						MemWrite <= '0';
						RegWrite <= '0';
						RegDat <= '1';
						ALUOp <= operacion;
					when "10" =>
						MemWrite <= '1';
						RegWrite <= '0';
						RegDat <= '0';
						ALUOp <= operacion;
					when "11" =>
						MemWrite <= '0';
						RegWrite <= '1';
						RegDat <= '0';
						ALUOp <= operacion;
				end case;
	end process;
end Behavioral;

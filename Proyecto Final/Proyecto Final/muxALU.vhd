library IEEE;
	
use ieee.std_logic_1164.all;

use ieee.numeric_std.all;

entity muxALU is
    port(
		-- Input ports
		selector : in std_logic;
		constanteInstruccion : std_logic_vector(6 downto 0) ; -- depende del tamaño de los datos que vaya a usar
		datoRegistro : in std_logic_vector(6 downto 0); -- depende del tamaño de los datos que vaya a usar
		
		-- Output ports
		datoElegido : out std_logic_vector(6 downto 0) -- depende del tamaño de los datos que vaya a usar
	 );
end muxALU;

architecture Behavioral of muxALU is

signal signalSelector : integer :=0;

begin
	signalSelector <= 1 when (selector = '1') else 0;
	
	with signalSelector select
		datoElegido <= constanteInstruccion when 0,
							datoRegistro when others;
							
end Behavioral;


library IEEE;
	
use ieee.std_logic_1164.all;

use ieee.numeric_std.all;

entity muxMemoryIorD is
    port(
		-- Input ports
		selector : in std_logic;
		instruccion : in natural range 0 to 63 ; -- depende del tamaño de los datos que vaya a usar
		dato : in std_logic_vector(6 downto 0); -- depende del tamaño de los datos que vaya a usar
		
		-- Output ports
		datoElegido : out natural range 0 to 63 -- depende del tamaño de los datos que vaya a usar
	 );
end muxMemoryIorD;

architecture Behavioral of muxMemoryIorD is

signal signalSelector : integer :=0;
signal datoALUOut : natural range 0 to 63; -- Toca convertirlo a natural

begin
	datoALUOut <= to_integer(unsigned(dato));
	signalSelector <= 1 when (selector = '1') else 0;
	
	with signalSelector select
		datoElegido <= instruccion when 0,
							datoALUOut when others;
							
end Behavioral;
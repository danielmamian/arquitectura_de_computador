-- A library clause declares a name as a library.  It 
-- does not create the library; it simply forward declares 
-- it. 
library IEEE;
	-- STD_LOGIC and STD_LOGIC_VECTOR types, and relevant functions
	use ieee.std_logic_1164.all;

	-- SIGNED and UNSIGNED types, and relevant functions
	use ieee.numeric_std.all;

entity registerFile is
	port
	(
		-- Input ports
		rd	: in  std_logic_vector(3 downto 0);-- Voy a ulizar 9 registros por lo que debo utilizar 4 bits
		rt	: in  std_logic_vector(3 downto 0);
		rs	: in  std_logic_vector(3 downto 0);
		dataIn : in std_logic_vector(6 downto 0); -- Se define por el dato mas grande que pase por ahi, en este caso seria le medicion del sensor de luz
		writeEneable : in std_logic;
		clk	: in  std_logic;
		
		-- Output ports
		rOutA	: out std_logic_vector(6 downto 0); -- depende del tamaño de los datos que vaya a usar
		rOutB	: out std_logic_vector(6 downto 0) -- depende del tamaño de los datos que vaya a usar
	);
end registerFile;


architecture Behavioral of registerFile is
-- Se añaden tantos registros como se vayan a usar
-- depende del tamaño de los datos que vaya a usar
	signal R0 : std_logic_vector(6 downto 0) := "0000000"; 
	signal R1 : std_logic_vector(6 downto 0) := "0000001"; 
	signal R2 : std_logic_vector(6 downto 0) := "0000001"; 
	signal R3 : std_logic_vector(6 downto 0); 
	signal R4 : std_logic_vector(6 downto 0); 
	signal R5 : std_logic_vector(6 downto 0); 
	signal R6 : std_logic_vector(6 downto 0); 
	signal R7 : std_logic_vector(6 downto 0); 
	signal R8 : std_logic_vector(6 downto 0); 
	signal R9 : std_logic_vector(6 downto 0); 
	signal R10 : std_logic_vector(6 downto 0); 
	signal R11 : std_logic_vector(6 downto 0); 
	signal R12 : std_logic_vector(6 downto 0); 
	signal R13 : std_logic_vector(6 downto 0); 
	signal R14 : std_logic_vector(6 downto 0); 
	signal R15 : std_logic_vector(6 downto 0); 
	
	signal selectorrt : integer range 0 to 15; 
	signal selectorrs : integer range 0 to 15; 
-------------------------------------------------------
	begin
		process(clk)
			begin
				if (rising_edge(clk)) then
					if (writeEneable ='1') then
							case rd is 
								when "0000" =>
									R0 <= dataIn;
								when "0001" =>
									R1 <= dataIn;
								when "0010" =>
									R2 <= dataIn;
								when "0011" =>
									R3 <= dataIn;
								when "0100" =>
									R4 <= dataIn;
								when "0101" =>
									R5 <= dataIn;
								when "0110" =>
									R6 <= dataIn;
								when "0111" =>
									R7 <= dataIn;
								when "1000" =>
									R8 <= dataIn;
								when "1001" =>
									R9 <= dataIn;
								when "1010" =>
									R10 <= dataIn;
								when "1011" =>
									R11 <= dataIn;
								when "1100" =>
									R12 <= dataIn;
								when "1101" =>
									R13 <= dataIn;
								when "1110" =>
									R14 <= dataIn;
								when "1111" =>
									R15 <= dataIn;
							end case;
					end if;
				end if;
		end process;
		
		selectorrt <= to_integer(unsigned(rt));
		with selectorrt select
			rOutB <= R0 when 0,
						R1 when 1,
						R2 when 2,
						R3 when 3,
						R4 when 4,
						R5 when 5,
						R6 when 6,
						R7 when 7,
						R8 when 8,
						R9 when 9,
						R10 when 10,
						R11 when 11,
						R12 when 12,
						R13 when 13,
						R14 when 14,
						R15 when 15;
						
		selectorrs <= to_integer(unsigned(rs));
		with selectorrs select
			rOutA <= R0 when 0,
						R1 when 1,
						R2 when 2,
						R3 when 3,
						R4 when 4,
						R5 when 5,
						R6 when 6,
						R7 when 7,
						R8 when 8,
						R9 when 9,
						R10 when 10,
						R11 when 11,
						R12 when 12,
						R13 when 13,
						R14 when 14,
						R15 when 15;
		
end Behavioral;

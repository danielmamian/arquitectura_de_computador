library verilog;
use verilog.vl_types.all;
entity ALU is
    port(
        opeA            : in     vl_logic_vector(31 downto 0);
        opeB            : in     vl_logic_vector(31 downto 0);
        operacion       : in     vl_logic_vector(2 downto 0);
        respuesta       : out    vl_logic_vector(31 downto 0)
    );
end ALU;

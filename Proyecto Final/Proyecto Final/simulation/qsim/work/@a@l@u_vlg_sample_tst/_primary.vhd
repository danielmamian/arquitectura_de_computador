library verilog;
use verilog.vl_types.all;
entity ALU_vlg_sample_tst is
    port(
        opeA            : in     vl_logic_vector(31 downto 0);
        opeB            : in     vl_logic_vector(31 downto 0);
        operacion       : in     vl_logic_vector(2 downto 0);
        sampler_tx      : out    vl_logic
    );
end ALU_vlg_sample_tst;

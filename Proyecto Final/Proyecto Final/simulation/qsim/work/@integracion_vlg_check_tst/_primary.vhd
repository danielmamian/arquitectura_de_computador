library verilog;
use verilog.vl_types.all;
entity Integracion_vlg_check_tst is
    port(
        inst            : in     vl_logic_vector(20 downto 0);
        queesA          : in     vl_logic_vector(6 downto 0);
        queesB          : in     vl_logic_vector(6 downto 0);
        queesrd         : in     vl_logic_vector(3 downto 0);
        queesrs         : in     vl_logic_vector(3 downto 0);
        queesrt         : in     vl_logic_vector(3 downto 0);
        salidaResultado : in     vl_logic_vector(6 downto 0);
        sampler_rx      : in     vl_logic
    );
end Integracion_vlg_check_tst;

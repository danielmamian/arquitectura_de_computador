library verilog;
use verilog.vl_types.all;
entity Integracion_vlg_sample_tst is
    port(
        clk_in          : in     vl_logic;
        entradaPC       : in     vl_logic_vector(5 downto 0);
        muxALUSelector  : in     vl_logic;
        muxIorDSelector : in     vl_logic;
        opALU           : in     vl_logic_vector(3 downto 0);
        weALUOut        : in     vl_logic;
        weIR            : in     vl_logic;
        weRA            : in     vl_logic;
        weRAM           : in     vl_logic;
        weRB            : in     vl_logic;
        weRF            : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end Integracion_vlg_sample_tst;

library verilog;
use verilog.vl_types.all;
entity controlUnit is
    port(
        op              : in     vl_logic_vector(1 downto 0);
        MemWrite        : out    vl_logic;
        RegWrite        : out    vl_logic;
        RegDat          : out    vl_logic;
        ALUOp           : out    vl_logic_vector(1 downto 0)
    );
end controlUnit;

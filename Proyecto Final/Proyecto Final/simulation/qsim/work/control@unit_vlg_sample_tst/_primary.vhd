library verilog;
use verilog.vl_types.all;
entity controlUnit_vlg_sample_tst is
    port(
        op              : in     vl_logic_vector(1 downto 0);
        sampler_tx      : out    vl_logic
    );
end controlUnit_vlg_sample_tst;

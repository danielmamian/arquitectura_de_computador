library verilog;
use verilog.vl_types.all;
entity registerFile is
    port(
        rd              : in     vl_logic_vector(1 downto 0);
        rt              : in     vl_logic_vector(1 downto 0);
        rs              : in     vl_logic_vector(1 downto 0);
        dataIn          : in     vl_logic_vector(31 downto 0);
        writeEneable    : in     vl_logic;
        clk             : in     vl_logic;
        rOutA           : out    vl_logic_vector(31 downto 0);
        rOutB           : out    vl_logic_vector(31 downto 0)
    );
end registerFile;

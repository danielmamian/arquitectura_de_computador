library IEEE;
	
use ieee.std_logic_1164.all;

use ieee.numeric_std.all;

entity registroAB is
    port(
		-- Input ports
		entradaDato : in std_logic_vector(6 downto 0);
		writeEneable : in std_logic;
		clk : in std_logic;
		
		-- Output ports
		salidaDato : out std_logic_vector(6 downto 0) -- depende del tamaño de los datos que vaya a usar
	 );
end registroAB;

architecture Behavioral of registroAB is

signal data : std_logic_vector(6 downto 0);

begin
	process(clk)
	begin
		if(rising_edge(clk) and writeEneable = '1') then
			data <= entradaDato;
		end if;
	end process;
	
	salidaDato <= data;
							
end Behavioral;
onerror {quit -f}
vlib work
vlog -work work cultivos.vo
vlog -work work cultivos.vt
vsim -novopt -c -t 1ps -L cycloneiv_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate work.Integracion_vlg_vec_tst
vcd file -direction cultivos.msim.vcd
vcd add -internal Integracion_vlg_vec_tst/*
vcd add -internal Integracion_vlg_vec_tst/i1/*
add wave /*
run -all

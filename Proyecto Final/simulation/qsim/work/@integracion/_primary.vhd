library verilog;
use verilog.vl_types.all;
entity Integracion is
    port(
        clk_in          : in     vl_logic;
        weRF            : in     vl_logic;
        opALU           : in     vl_logic_vector(3 downto 0);
        muxALUSelector  : in     vl_logic;
        muxIorDSelector : in     vl_logic;
        entradaPC       : in     vl_logic_vector(5 downto 0);
        weIR            : in     vl_logic;
        weRAM           : in     vl_logic;
        weRA            : in     vl_logic;
        weRB            : in     vl_logic;
        weALUOut        : in     vl_logic;
        salidaResultado : out    vl_logic_vector(6 downto 0);
        queesrs         : out    vl_logic_vector(3 downto 0);
        queesrt         : out    vl_logic_vector(3 downto 0);
        queesrd         : out    vl_logic_vector(3 downto 0);
        queesA          : out    vl_logic_vector(6 downto 0);
        queesB          : out    vl_logic_vector(6 downto 0);
        inst            : out    vl_logic_vector(20 downto 0)
    );
end Integracion;

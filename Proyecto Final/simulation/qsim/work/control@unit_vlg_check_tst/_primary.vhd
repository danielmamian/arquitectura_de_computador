library verilog;
use verilog.vl_types.all;
entity controlUnit_vlg_check_tst is
    port(
        ALUOp           : in     vl_logic_vector(1 downto 0);
        MemWrite        : in     vl_logic;
        RegDat          : in     vl_logic;
        RegWrite        : in     vl_logic;
        sampler_rx      : in     vl_logic
    );
end controlUnit_vlg_check_tst;

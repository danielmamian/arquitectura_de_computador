library verilog;
use verilog.vl_types.all;
entity registerFile_vlg_check_tst is
    port(
        rOutA           : in     vl_logic_vector(31 downto 0);
        rOutB           : in     vl_logic_vector(31 downto 0);
        sampler_rx      : in     vl_logic
    );
end registerFile_vlg_check_tst;

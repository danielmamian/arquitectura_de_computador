library verilog;
use verilog.vl_types.all;
entity registerFile_vlg_sample_tst is
    port(
        clk             : in     vl_logic;
        dataIn          : in     vl_logic_vector(31 downto 0);
        rd              : in     vl_logic_vector(1 downto 0);
        rs              : in     vl_logic_vector(1 downto 0);
        rt              : in     vl_logic_vector(1 downto 0);
        writeEneable    : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end registerFile_vlg_sample_tst;
